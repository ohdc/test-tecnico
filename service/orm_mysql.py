#!/usr/bin/python3

import sys
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Sequence
from sqlalchemy import create_engine
engine = create_engine('mysql://user:passwd@localhost/BD')

print (sqlalchemy.__version__ )

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

#Column(Integer, Sequence('user_id_seq'), primary_key=True)

from sqlalchemy import Column, Integer, String

class Datamonitors(Base):
     __tablename__ = 'datamonitors'

     id = Column(Integer, Sequence('id'), primary_key=True)
     data = Column(String)
     #fullname = Column(String)
     #nickname = Column(String)

     def __repr__(self):
        return "<Datamonitors(data='%s')>" % (
                             self.data)

Base.metadata.create_all(engine)


def sendDataBD( datamonin  ):
    Session = sessionmaker(bind=engine)
    Session.configure(bind=engine)
    session = Session()
    ed_datamonitors = Datamonitors(data=datamonin)
    session.add(ed_datamonitors)
    print ( session.commit() )

