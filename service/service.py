#!/usr/bin/python3


import random
import time
import datetime
import sys 
import os
import subprocess 
import json
import requests
from  orm_mysql  import sendDataBD

arrayRamT = []
arrayRamO = []

if len ( sys.argv[1:] ) != 1:
    print ( 'Usage: ' + sys.argv[0] + ' <frequency>' )
    sys.exit()

frequency=int(sys.argv[1])
Tfix  = 0
Ofix  = 0


while True:

    #Aca funciona con el path absoluto
    auxVar = subprocess.check_output ( '/PATH/devioT.py' )   

    #print ( type ( auxVar ) )    
    print ( auxVar ) 
    startjson = list( str ( auxVar )  ).index('{')
    print ( list( str ( auxVar )  ).index('}') )
    endjson  = list( str ( auxVar )  ).index('}')
    #print ( list( str ( aux )  ) )
    newaux = ''.join (   ( list( str(  auxVar ) )[startjson:endjson+1] )  )

    print ( newaux )

    auxdict = json.loads( newaux  )
    auxjson = json.dumps( auxdict )


    print ( type ( auxdict ))
    print ( auxdict['T'])
    print ( auxdict['O'])

    arrayRamT.append( float ( auxdict['T'] ) )
    arrayRamO.append( float ( auxdict['O'] ) ) 

    if len( arrayRamT ) == 10:
        print ( "Estos son los ultimos 10 valores de T: "  + str( arrayRamT ) ) 
        print ( sum( arrayRamT ) / len( arrayRamT ) )
        print ( sum ( arrayRamT[5:]  )  / len ( arrayRamT[5:] ) )
        prom10 = sum( arrayRamT ) / len( arrayRamT )
        promL5 = sum ( arrayRamT[5:]  )  / len ( arrayRamT[5:] ) 
        if prom10 < promL5:
            print ( "Abriendo valvula de rellenado de agua..." )
            print ( datetime.datetime.now() )
            print ( datetime.datetime.now().strftime('%H') )
            Tfix = 1
        arrayRamT.pop(0)

    if len( arrayRamO ) == 10:
        print ( "Estos son los ultimos 10 valores de O: "  + str( arrayRamO ) ) 
        print ( sum( arrayRamO ) / len( arrayRamO ) )
        print ( sum ( arrayRamO[5:]  )  / len ( arrayRamO[5:] ) )
        prom10 = sum( arrayRamO ) / len( arrayRamO )
        promL5 = sum ( arrayRamO[5:]  )  / len ( arrayRamO[5:] ) 
        if prom10 < promL5:
            print ( "Inyectando Oxígeno..." )
            print ( datetime.datetime.now() )
            print ( datetime.datetime.now().strftime('%H') )
            Ofix = 1
        arrayRamO.pop(0)
    print ('Xx' * 30 )

    auxdict['Tfix'] = Tfix  
    auxdict['Ofix'] = Ofix  
    auxdictstr1 = str ( auxdict )  
    auxdictstr2 = auxdictstr1.replace("\'", "\"")  
    print ( "Este es el dict string para enviar a la api: " + auxdictstr2  )  
    Tfix = 0 
    Ofix = 0
    #Se envia a la api
    response = requests.post('http://localhost:8000/api/datamonitor', data = { "data" :  auxdictstr2 } )
    print ( "Esto es lo que responde" ) 
    print ( response.text ) 
    #Se envia a la base de datos 
    sendDataBD( response.text )
    print ( "Esto es lo que responde" ) 
    time.sleep(frequency)


