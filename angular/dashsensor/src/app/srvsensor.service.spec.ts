import { TestBed } from '@angular/core/testing';

import { SrvsensorService } from './srvsensor.service';

describe('SrvsensorService', () => {
  let service: SrvsensorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SrvsensorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
