import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { fromEvent } from 'rxjs';
//import { range } from 'rxjs' 
import { map, filter } from 'rxjs/operators';
//import { map, retry, catchError } from 'rxjs/operators';
//import 'rxjs/operator/map'
//import  'rxjs/operator/map';
//import  'rxjs/Rx';


@Injectable({
  providedIn: 'root'
})
export class SrvsensorService {

  constructor(private _http: HttpClient) { }
  
  dailySensor() {
	//return this._http.get("http://localhost:8000/api/datamonitor").map(result => result);
	//return this._http.get("http://localhost:8000/api/datamonitor");
	//return this._http.get("http://10.110.53.204/api/datamonitor");
	return this._http.get("http://34.71.31.10/api/datamonitor");
  }
}
