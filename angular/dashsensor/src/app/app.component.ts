import { Component } from '@angular/core';
import { SrvsensorService } from './srvsensor.service';
//import { Chart } from 'chart.js';
import { Chart } from 'node_modules/chart.js/dist/chart.js';
//import { Chart } from './node_modules/chart.js/dist/chart.js';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dashsensor';
 
  chart = []; 	
  constructor(private _srvsensor: SrvsensorService) {}

  ngOnInit(){
	this._srvsensor.dailySensor()
	 	.subscribe(res => { 
			//let data_array_T = data_array.map(data_array => data_array.Tfix ) 
			//let data_array_T = data_array.map(data_array => data_array.Tfix ) 
			//console.log(data_array_T) 
			//let obj: aux = JSON.parse ( res[0]['data']) 
			//console.log(res[0]['data']['T']) 
			//console.log(aux) 
			//console.log(aux.T) 
			
			//let string2json = res[0].data  
			//let obj: MyObj = JSON.parse( string2json )
			//let obj = JSON.parse(res[0].data);
			
			let jsonObj = [] ;
			let jsonTHora = [] ;
			let jsonOHora = [] ;
			for ( var _i = 0; _i < res.length; _i++ ){
				let obj = JSON.parse(res[_i].data);
				jsonObj.push( obj )
				//console.log("hola XXXXXX" , obj.Tfix)  
			}
			//console.log(jsonObj[40].T)  
			//console.log(res)  
			//console.log(res.length)  
			let data_array_date  = res.map(res => res.created_at ) 
			let data_array_T = jsonObj.map(jsonObj => jsonObj.T ) 
			let data_array_O = jsonObj.map(jsonObj => jsonObj.O ) 
			let data_array_Tfix = jsonObj.map(jsonObj => jsonObj.Tfix ) 
			let data_array_Ofix = jsonObj.map(jsonObj => jsonObj.Ofix ) 
				

			//data_array_Tfix = JSON.stringify ( data_array_Tfix )  
			//let data_array_Tfix_data = JSON.parse ( data_array_Tfix )  


			//console.log("hola    " ,  data_array_Tfix_data[0]  )
			for (var _i = 0; _i < data_array_date.length; _i++){
				var aux = data_array_date[_i].substring(0,13) 
				jsonTHora[aux] = 0
				jsonOHora[aux] = 0
			}	
			for (var _i = 0; _i < data_array_date.length; _i++){
				var aux = data_array_date[_i].substring(0,13) 
				//console.log ( aux )
				//let objTfix = Number(data_array_Tfix[_i]) 
				let objOfix = data_array_Ofix[_i] 
				//console.log ( "hola   hola  ",  objOfix ) 
				jsonTHora[aux] += data_array_Tfix[_i]
				jsonOHora[aux] += data_array_Ofix[_i]
			}	

		   
			let data_array_FixHora  = Object.keys   ( jsonOHora )
			let data_array_TfixHora = Object.values ( jsonTHora )
			let data_array_OfixHora = Object.values ( jsonOHora )
			//console.log(jsonTHora) 
			//console.log(data_array_T) 
			//console.log(data_array_O) 
			//console.log(data_array_Tfix) 
			//console.log(data_array_Ofix) 
			//console.log(data_array_FixHora) 
			//console.log(data_array_TfixHora) 
			//console.log(data_array_OfixHora) 
			//console.log(data_array_date[0]) 

			var ctx = document.getElementById('myChart1').getContext('2d');

			this.chart  = new Chart(ctx, {
				type: 'line',
				data: {
					labels: data_array_date,
					datasets: [
						{
							label: 'Monitor T',
							data: data_array_T,
							borderColor: '#3cba9f',
							fill: false
						},		
						{
							label: 'Monitor O',
							data: data_array_O,
							borderColor: '#ffcc00',
							fill: false
						},		
					]
				},
				options: {
					 plugins: {
      title: {
        display: true,
        text: 'Monitor Sensores',
      }
    },
					legend: {
						display: false
						},

					scales:{
						xAxes: [{
							display: true
							}],
						yAxes: [{
							display: true
							}]	
						}	
					}	
			})  
			var ctxx = document.getElementById('myChart2').getContext('2d');

			this.chart  = new Chart(ctxx, {
				type: 'bar',
				data: {
					labels: data_array_FixHora,
					datasets: [
						{
							label: 'Fix  T',
							data: data_array_TfixHora,
							borderColor: '#0000ff',
							backgroundColor: '#0000ff'
						},		
						{
							label: 'Fix  O',
							data: data_array_OfixHora,
							borderColor: '#ffcc00',
							backgroundColor: '#ff0000'
						},		
					]
				},
				options: {
					 plugins: {
      title: {
        display: true,
        text: 'Eventos Por Hora',
      }
    },
					legend: {
						display: false
						},
					scales:{
						xAxes: [{
							display: true
							}],
						yAxes: [{
							display: true
							}]	
						}	
					}	
			})  
		})
  }
}
