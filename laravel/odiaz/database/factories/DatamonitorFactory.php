<?php

namespace Database\Factories;

use App\Models\Datamonitor;
use Illuminate\Database\Eloquent\Factories\Factory;

class DatamonitorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Datamonitor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
