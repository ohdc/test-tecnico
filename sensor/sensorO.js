const Sensor = require('./sensor.js')
let  sensor = new Sensor('Oxigeno')
const Net = require('net');
const ip = require('ip');
console.log(ip.address())
const port = 65100;
const host = ip.address();
var vmin = 6
var vmax = 15

const server = new Net.Server();
// The server listens to a socket for a client to make a connection request.
// Think of a socket as an end point.
server.listen( {host, port}, function() {
    console.log(`Server listening for connection requests on socket ${host}:${port}`);
});

// When a client requests a connection with the server, the server creates a new
// socket dedicated to that client.
server.on('connection', function(socket) {
    console.log('A new connection has been established.');

    // Now that a TCP connection has been established, the server can send data to
    // the client by writing to its soblecket.
    socket.write('' +  sensor.getRndDouble(vmin, vmax) );

    // When the client requests to end the TCP connection with the server, the server
    // ends the connection.
    socket.on('end', function() {
        console.log('Closing connection with the client');
    });

    // Don't forget to catch error, for your own sake.
    socket.on('error', function(err) {
        console.log(`Error: ${err}`);
    });
});
